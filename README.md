Ada Examples
============

I have a long term vision to write software for space technology. Right now I'm a very experienced professional
programmer, but I have no experience in critical systems programming, or using the Ada language. This repository is the
beginning of my journey to writing software to launch into space.

Right now I'm writing code in vim (you can find my config in my [Bitbucket repo](https://bitbucket.org/eviljonny/vimconfig)).
I'm using the [GNAT](https://en.wikipedia.org/wiki/GNAT) [compiler for OSX from Adacore](https://www.adacore.com/download)

This repo is a place for me to store the examples I'm writing following the [tutorial written by David A.
Wheeler](http://www.adahome.com/Tutorials/Lovelace/)

Current place in the tutorial:  http://www.adahome.com/Tutorials/Lovelace/s6s3.htm

Bibliography
------------

* [Ada95 Lovelace Tutorial](http://www.adahome.com/Tutorials/Lovelace/)
* [Backus-Naur Form](http://www.adahome.com/Tutorials/Lovelace/bnf.htm)
* [Ada95 Quality and Style Guide](http://www.adaic.org/resources/add\_content/docs/95style/html/cover.html)

