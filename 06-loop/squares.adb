-- Print the square of whatever the user enters, unless they enter 0, then exit
with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada.Text_IO, Ada.Integer_Text_IO;

procedure Squares is
  X : Integer;
begin
  loop
    Put("Enter number to square: ");
    Get(X);
  exit when X = 0;
    Put(X * X);
    New_Line;
  end loop;

  Put("Thank you, goodbye.");
end Squares;
