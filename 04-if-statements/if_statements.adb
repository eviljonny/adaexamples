-- Simple if statements
with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada.Text_IO, Ada.Integer_Text_IO;

procedure If_Statements is
  A : Integer := 5;
  B : Integer := 6;
begin
  if A > B then
    Put("A > B");
  elsif A < B then
    Put("A < B");
  else
    Put("A = B");
  end if;
end If_Statements;
