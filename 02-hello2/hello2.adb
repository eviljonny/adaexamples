-- Print a simple message using a use clause to find the functions
with Ada.Text_IO;
use Ada.Text_IO; -- Auto search in Ada.Text_IO for functions
procedure Hello2 is
begin
  Put_Line("Hello world!");
end;
