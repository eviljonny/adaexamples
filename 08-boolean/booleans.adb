-- Boolean operations
with Ada.Text_IO;
use Ada.Text_IO;

procedure Booleans is
  function Test_Short_Circuit(X : in Boolean; name : in String) return Boolean is
  begin
    Put("Evaluated part 2 of " & name);
    New_Line;
    Return X;
  end Test_Short_Circuit;

  X : Boolean := False;
begin
  X := True or Test_Short_Circuit(True, "True or True");
  X := True or else Test_Short_Circuit(True, "True or else True");

  X := False and Test_Short_Circuit(True, "False and True");
  X := False and then Test_Short_Circuit(True, "True and then True");

  X := not False and Test_Short_Circuit(True, "not False and True");
  X := not False and then Test_Short_Circuit(True, "not False and then True");
  X := not True and Test_Short_Circuit(True, "not True and True");
  X := not True and then Test_Short_Circuit(True, "not True and then True");

  X := True xor Test_Short_Circuit(True, "True xor True");
  X := False xor Test_Short_Circuit(True, "False xor True");
  X := False xor Test_Short_Circuit(False, "False xor False");

  case X is
    when True => Put("X is True");
    when False => Put("X is False");
  end case;

  New_Line;

end Booleans;
