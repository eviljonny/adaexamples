-- Floats are arbitrary precision (which you can't specify)
with Ada.Float_Text_IO;
use Ada.Float_Text_IO;

procedure Float_Type is
  A, B : Float := 0.0;
  I, J : Integer := 1;
begin
  A := B + 8.0;
  I := J * 3;
  B := Float(I) + A;
  Put(B);
end Float_Type;
