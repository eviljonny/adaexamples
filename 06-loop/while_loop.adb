-- While loop
with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada.Text_IO, Ada.Integer_Text_IO;

procedure While_Loop is
  X : Integer := 0;
begin
  while X < 10
  loop
    Put("Loop ");
    Put(X);
    New_Line;
    X := X + 1;
  end loop;
end While_Loop;
