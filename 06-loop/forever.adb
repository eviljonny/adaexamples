-- Loop example looping forever
with Ada.Text_IO;
use Ada.Text_IO;

procedure Forever is
begin
  loop
    Put("Hello");
  end loop;
end Forever;
