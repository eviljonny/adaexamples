-- Print a simple message to demo an Ada program
with Ada.Text_IO;

procedure Hello is
begin
  Ada.Text_IO.Put_Line("Hello world!");
end Hello;
