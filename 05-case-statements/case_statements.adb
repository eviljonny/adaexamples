-- Simple case statement example
with Ada.Text_IO;
use Ada.Text_IO;

procedure Case_Statements is
  A : Integer := 3;
begin
  case A is
    when 0..5   => 
      Put("< 5");
      New_Line;
      Put("Second Line");
    when 6..10  => Put("6..10");
    when others => Put("Others");
  end case;
end Case_Statements;
