-- Reverse for loop
with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada.Text_IO, Ada.Integer_Text_IO;

procedure For_Loop_Reverse is
begin
  for Index in reverse 1..20
  loop
    Put("Iteration ");
    Put(Index);
    New_Line;
  end loop;
end For_Loop_Reverse;
